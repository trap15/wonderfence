# WonderFence

*WonderFence* is a software implementation of the *MobileWonderGate* adapter unit used by various WonderSwan software to communicate with an NTT DoCoMo PDC cellular phone. Keep in mind that this is a work-in-progress project, and the code is very messy and probably very buggy right now. This should improve over time.

## Supported Core Functionality

This implementation supports the following *MobileWonderGate* commands:

- Simulate adapter power on/off and status modes
- Simulate PDC reception and status
- Phone number dialing and hanging up (presently simulates an accepted call, but nothing farther)
- Setting PPP and DNS information (presently ignores this information)
- Most socket functionality:
    - Creating a socket
    - Connecting to a socket
    - Reading from and writing to a socket
    - Closing a socket
- Retrieving canonical hostname and IP address

## Supported Software

This list is currently incomplete, and will be filled as more software is tested:

- *MobileWonderGate* mopera cart
    - Requires a re-implementation of two servers (*bpl01.mopera.ne.jp* and *bpl02.mopera.ne.jp*).
    - It is possible to see a webpage without the re-implementation if you go to the *Mailer*'s options and select setup quick-start.
- *Terrors 2*
    - Requires a re-implementation of the server (*terrors2.wgg.channel.or.jp*).

# Using

*WonderFence* is designed to be used with the RS232-WonderSwan serial adapter which is included with the WonderWitch package. Any other hardware configuration is unsupported and may or may not work. If you are able to get another setup working, please contact me as I'd be very interested in hearing other working setups.

*WonderFence* as it is currently written interfaces to the serial port through stdin and stdout. The method I use to funnel the serial port through them is to register *WonderFence* as a file transfer application in *minicom*, then run it from there. I believe a similar method may be used with *screen*, but I haven't tested this yet.

## Serial Port Configuration

The *MobileWonderGate* by default communicates at *9600 baud*. The hardware does support *38400 baud*, but the hand-shaking required to step up the baudrate is currently unknown to me.

Your serial port should be configured as 9600/8-N-1 (9600 baud, 8 data bits, no parity, 1 stop bit).

## Configuration File

*WonderFence* supports a configuration file (INI format), which may be either specified as a command line argument, or will default to *wonfence.ini* in the current working directory. If the file cannot be found, defaults will be used.

The fields defined in the configuration file are:

- pdc.reception: PDC reception level
    - Default: 15
    - Valid range: 0 through 15
- ppp.user: Default PPP username
    - Default: (none)
    - This is currently unused.
- ppp.pass: Default PPP password
    - Default: (none)
    - This is currently unused.

The configuration file also allows specification of *hostname and socket hacks*. These allow redirection of hostnames and socket connections, for replacing dead servers.

See the included *wonfence.ini* as a sample configuration file, which also includes the syntax for specifying the hostname and socket hacks.

# Cool Links

- <http://daifukkat.su/> - My website
- <https://bitbucket.org/trap15/wonderfence> - This repository

# Greetz

- LiraNuna - Helping me out big-time, and pushing me to buy a WonderWitch ;D
- Ryphecha - Always helpful, and Mednafen is fantastic
- rancor - Your ultra-cheap WonderSwan has ruined my life
- \#raidenii - Forever impossible
- Gunpei Yokoi - A true idol, may he rest in peace.
- Qute Corporation - WonderWitch is a fantastic set of HW/SW, well done.
- Bandai
- French Roast Coffee

# Licensing

All contents within this repository (unless otherwise specified) are licensed under the following terms:

The MIT License (MIT)

Copyright (C)2014 Alex 'trap15' Marshall

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
