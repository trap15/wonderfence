#include "top.h"

int serial_rx_handler(tser_t* ts, void* data)
{
  wonfence_t* fence = data;
  int err;
  wongate_cmd_t* cmd;
  wongate_cmd_t* repl;

  cmd = wongate_cmd_new();

  /* Receive full command */
  err = wongate_ser_recv(ts, cmd);
  if(err < 0) goto _fail;

  /* Handle the command and create the reply command */
  err = wonfence_handle_cmd(fence, cmd, &repl);
  if(err < 0) goto _fail;

  /* We're done with the initial command, delete it */
  wongate_cmd_free(cmd);

  /* If there's a reply, send it */
  if(repl != NULL) {
    err = wongate_ser_send(ts, repl);
    if(err < 0) goto _fail;

    wongate_cmd_free(repl);
  }

  err = 0;
_fail:
  if(err < 0) /* Quit on error */
    return 0;
  else
    return 1;
}

int main(int argc, const char *argv[])
{
  const char* conf_fn;
  tser_t ts;
  tser_errcode err;
  int ret = EXIT_FAILURE;
  wonfence_t* fence;

  if(argc > 1)
    conf_fn = argv[1];
  else
    conf_fn = "wonfence.ini";

  /* Initialize serial communication stuff */
  tser_init(&ts);

  fprintf(stderr, "WonderFence v1.00 (C)2014 Alex 'trap15' Marshall\n");

  /* Create WonderFence */
  fence = wonfence_new();
  err = wonfence_load_config(fence, conf_fn);
  if(err < 0) {
    fprintf(stderr, "Unable to load configuration file %s. Using defaults.\n", conf_fn);
    err = 0;
  }

  /* Setup the handlers */
  tser_set_callback_data(&ts, fence);
  err = tser_set_receiver(&ts, serial_rx_handler);
  if(err < 0) goto _fail;

  /* Main serial loop */
  fprintf(stderr, "Starting mainloop!\n");
  tser_mainloop(&ts);

  /* Clean up time */
  fprintf(stderr, "Leaving!\n");

  wonfence_free(fence);

  /* Finish serial communication stuff */
  tser_finish(&ts);

  ret = EXIT_SUCCESS;
_fail:
  return ret;
}

