#include "tser.h"

tser_errcode tser_init(tser_t* ts)
{
  tser_errcode err = TSER_ERR_UNK;

  err = tser_set_read_fd(ts, -1);
  if(err < 0) goto _fail;
  err = tser_set_write_fd(ts, -1);
  if(err < 0) goto _fail;

  err = tser_set_callback_data(ts, NULL);
  if(err < 0) goto _fail;
  err = tser_set_periodic(ts, 0, NULL);
  if(err < 0) goto _fail;
  err = tser_set_receiver(ts, NULL);
  if(err < 0) goto _fail;

  ts->loop = ev_default_loop(EVBACKEND_POLL | EVBACKEND_SELECT);

  err = TSER_ERR_NONE;
_fail:
  return err;
}

tser_errcode tser_finish(tser_t* ts)
{
  tser_errcode err = TSER_ERR_UNK;

  err = TSER_ERR_NONE;
_fail:
  return err;
}

static void _tser_rx_cb(EV_P_ struct ev_io *w, int revents)
{
  tser_t* ts = w->data;

  if(ts->rx_cb != NULL) {
    if(!ts->rx_cb(ts, ts->cb_data)) {
      ev_io_stop(EV_A_ w);
      ev_unloop(EV_A_ EVUNLOOP_ALL);
    }
  }
}

static void _tser_tmr_cb(EV_P_ struct ev_timer *w, int revents)
{
  tser_t* ts = w->data;

  if(ts->tmr_cb != NULL) {
    if(!ts->tmr_cb(ts, ts->cb_data)) {
      ev_timer_stop(EV_A_ w);
      ev_unloop(EV_A_ EVUNLOOP_ALL);
    }
  }
}

tser_errcode tser_mainloop(tser_t* ts)
{
  tser_errcode err = TSER_ERR_UNK;

  ev_tstamp tmr_wait = ((ev_tstamp)ts->tmr_time) / 1000000.0;

  /* Setup the serial receive watcher */
  if(ts->rx_cb != NULL) {
    ev_io_init(&ts->ser_rx_watcher, _tser_rx_cb, ts->fds[0], EV_READ);
    ev_io_start(ts->loop, &ts->ser_rx_watcher);
    ts->ser_rx_watcher.data = ts;
  }

  /* Setup the timer watcher */
  if(ts->tmr_time != 0 && ts->tmr_cb != NULL) {
    ev_timer_init(&ts->time_watcher, _tser_tmr_cb, tmr_wait, tmr_wait);
    ev_timer_start(ts->loop, &ts->time_watcher);
    ts->time_watcher.data = ts;
  }

  /* Dive into the mainloop */
  ev_loop(ts->loop, 0);

  err = TSER_ERR_NONE;
_fail:
  return err;
}

tser_errcode tser_set_callback_data(tser_t* ts, void* data)
{
  tser_errcode err = TSER_ERR_UNK;

  ts->cb_data = data;

  err = TSER_ERR_NONE;
_fail:
  return err;
}

tser_errcode tser_set_periodic(tser_t* ts, unsigned long time, int (*callback)(tser_t* ts, void* data))
{
  tser_errcode err = TSER_ERR_UNK;

  ts->tmr_time = time;
  ts->tmr_cb = callback;

  err = TSER_ERR_NONE;
_fail:
  return err;
}

tser_errcode tser_set_receiver(tser_t* ts, int (*callback)(tser_t* ts, void* data))
{
  tser_errcode err = TSER_ERR_UNK;

  ts->rx_cb = callback;

  err = TSER_ERR_NONE;
_fail:
  return err;
}

tser_errcode tser_set_read_fd(tser_t* ts, int fd)
{
  tser_errcode err = TSER_ERR_UNK;

  if(fd == -1)
    fd = STDIN_FILENO;

  ts->fds[0] = fd;
  if(ts->fds[0] < 0)
    goto _fail;

  err = TSER_ERR_NONE;
_fail:
  return err;
}

tser_errcode tser_set_write_fd(tser_t* ts, int fd)
{
  tser_errcode err = TSER_ERR_UNK;

  if(fd == -1)
    fd = STDOUT_FILENO;

  ts->fds[1] = fd;
  if(ts->fds[1] < 0)
    goto _fail;

  err = TSER_ERR_NONE;
_fail:
  return err;
}
