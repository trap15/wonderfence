#ifndef TSER_H_
#define TSER_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <stdarg.h>

#include <ev.h>

typedef struct tser_t tser_t;
struct tser_t {
  /* tser stuff */
  FILE* logfp;
  int fds[2];

  /* libev stuff */
  struct ev_loop *loop;
  ev_io ser_rx_watcher;
  ev_timer time_watcher;

  void* cb_data;

  unsigned long tmr_time;
  int (*tmr_cb)(tser_t* ts, void* data);

  int (*rx_cb)(tser_t* ts, void* data);
};

#include "tser_err.h"
#include "tser_ser.h"

/* Initialize serial */
tser_errcode tser_init(tser_t* ts);
/* Finish serial */
tser_errcode tser_finish(tser_t* ts);

/* Mainloop */
tser_errcode tser_mainloop(tser_t* ts);

/* Set callback data */
tser_errcode tser_set_callback_data(tser_t* ts, void* data);
/* Set 'periodic' timer stuff */
/* Time is in microseconds */
tser_errcode tser_set_periodic(tser_t* ts, unsigned long time, int (*callback)(tser_t* ts, void* data));
/* Set 'data received' watcher stuff */
tser_errcode tser_set_receiver(tser_t* ts, int (*callback)(tser_t* ts, void* data));

/* Set reading file descriptor (default: stdin) */
tser_errcode tser_set_read_fd(tser_t* ts, int fd);
/* Set writing file descriptor (default: stdout) */
tser_errcode tser_set_write_fd(tser_t* ts, int fd);

#endif
