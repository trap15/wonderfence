#include "tser.h"

void tser_perror(tser_t* ts, tser_errcode err)
{
  switch(err) {
    case TSER_ERR_NONE:
      fprintf(stderr, "No error.\n");
      break;

    default:
      if(err < 0) {
        fprintf(stderr, "Unknown error.\n");
      }else{
        fprintf(stderr, "Unknown non-error.\n");
      }
      break;
    case TSER_ERR_UNK:
      fprintf(stderr, "Unknown error.\n");
      break;

    /* Serial port */
    case TSER_ERR_SERPORT_RX:
      fprintf(stderr, "Error in serial port rx.\n");
      break;
    case TSER_ERR_SERPORT_TX:
      fprintf(stderr, "Error in serial port tx.\n");
      break;
    case TSER_ERR_SERPORT_CHECK:
      fprintf(stderr, "Error in serial port check.\n");
      break;
  }
}

