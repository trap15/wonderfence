#include "top.h"

/* Power-on WonderGate */
WONFENCE_HANDLER(cmd0102_poweron)
{
  int err = -1;

  repl->size = 2;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_word(repl, 0, wongate_poweron(gate));

  err = 0;
_fail:
  return err;
}

/* Power-off WonderGate */
WONFENCE_HANDLER(cmd0FFF_poweroff)
{
  int err = -1;

  if((cmd->size < 4) ||
     (wongate_cmd_getparam_byte(cmd, 0) != 0x55) ||
     (wongate_cmd_getparam_byte(cmd, 1) != 0xAA) ||
     (wongate_cmd_getparam_byte(cmd, 2) != 0x55) ||
     (wongate_cmd_getparam_byte(cmd, 3) != 0xAA)) {
    /* Bad confirmation sequence, don't reply */
    repl->zero = 0x80;

    dbgprintf("Power-off command with bad confirmation!\n");
  }else{
    repl->size = 1;
    wongate_cmd_adjust(repl);
    wongate_cmd_setparam_byte(repl, 0, wongate_poweroff(gate));
  }

  err = 0;
_fail:
  return err;
}

WONFENCE_HANDLER(cmd0201_get_status)
{
  int err = -1;
  BYTE hdr[1];

  hdr[0] = wongate_cmd_getparam_byte(cmd, 0);

  if(hdr[0] != 0x03) {
    dbgprintf("Get Status command with bad header!\n");
    dbgprintf("Expected %02X, got %02X!\n", 0x03, hdr[0]);
  }

  repl->cmd = 0x02; /* why? */
  repl->size = 1;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, wongate_status(gate));

  err = 0;
_fail:
  return err;
}

/* Check PDC */
WONFENCE_HANDLER(cmd0100_check_pdc)
{
  int err = -1;

  repl->size = 2;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, wongate_pdc_status(gate));
  wongate_cmd_setparam_byte(repl, 1, wongate_pdc_reception(gate));

  err = 0;
_fail:
  return err;
}

/* Set PPP login */
WONFENCE_HANDLER(cmd0110_set_ppp_login)
{
  int err = -1;
  char* user;
  char* pass;
  BYTE sz;
  WORD adr;

  if((wongate_cmd_getparam_byte(cmd, 0) != 0x07) ||
      /* Dunno if we need to check 1-4? */
     (wongate_cmd_getparam_byte(cmd, 5) != 0x01)) {
    dbgprintf("Set PPP login with bad header!\n");
  }

  adr = 6;
  /* Get user name */
  sz = wongate_cmd_getparam_byte(cmd, adr++);
  if(sz != 0) {
    user = malloc(sz+1);
    memcpy(user, cmd->param + adr, sz);
    adr += sz;
    user[sz] = '\0';
  }else{
    user = NULL;
  }
  /* Get password */
  sz = wongate_cmd_getparam_byte(cmd, adr++);
  if(sz != 0) {
    pass = malloc(sz+1);
    memcpy(pass, cmd->param + adr, sz);
    adr += sz;
    pass[sz] = '\0';
  }else{
    pass = NULL;
  }

  repl->size = 1;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, wongate_set_ppp_login(gate, user, pass));

  if(user != NULL) free(user);
  if(pass != NULL) free(pass);

  err = 0;
_fail:
  return err;
}

WONFENCE_HANDLER(cmd0111_set_dns)
{
  int err = -1;
  BYTE dns1[4], dns2[4];
  int i;

  for(i = 0; i < 4; i++) {
    dns1[i] = wongate_cmd_getparam_byte(cmd, 0x9+i);
    dns2[i] = wongate_cmd_getparam_byte(cmd, 0xD+i);
  }

  repl->size = 1;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, wongate_set_dns(gate, dns1, dns2));

  err = 0;
_fail:
  return err;
}

/* Connect to phone line, start PPP */
WONFENCE_HANDLER(cmd0108_dialup)
{
  int err = -1;
  char* num;
  BYTE sz;
  WORD adr;

  BYTE hdr[3];

  hdr[0] = wongate_cmd_getparam_byte(cmd, 0);
  hdr[1] = wongate_cmd_getparam_byte(cmd, 1);
  hdr[2] = wongate_cmd_getparam_byte(cmd, 2);

  if((hdr[0] != 0x00) ||
     (hdr[1] != 0x01) ||
     (hdr[2] != 0x03)) {
    dbgprintf("Dial up with bad header!\n");
    dbgprintf("Expected %02X,%02X,%02X, got %02X,%02X,%02X\n", 0x00,0x01,0x03, hdr[0],hdr[1],hdr[2]);
  }

  adr = 3;
  /* Get phone number */
  sz = wongate_cmd_getparam_byte(cmd, adr++);
  if(sz != 0) {
    num = malloc(sz+1);
    memcpy(num, cmd->param + adr, sz);
    adr += sz;
    num[sz] = '\0';
  }else{
    num = NULL;
  }

  repl->cmd = 0x0B; /* why? */
  repl->size = 1;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, wongate_dial(gate, num));

  if(num != NULL) free(num);

  err = 0;
_fail:
  return err;
}

/* Hang up */
WONFENCE_HANDLER(cmd010A_hangup)
{
  int err = -1;

  if((wongate_cmd_getparam_byte(cmd, 0) != 0x01)) {
    dbgprintf("Hang up with bad header!\n");
  }

  repl->cmd = 0x0B; /* why? */
  repl->size = 1;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, wongate_hangup(gate));

  err = 0;
_fail:
  return err;
}

WONFENCE_HANDLER(cmd1108_gethost)
{
  int err = -1;
  char* name;
  char* newname;
  WORD sz;
  BYTE res;
  uint8_t ipadr[4];

  sz = 0;
  while(wongate_cmd_getparam_byte(cmd, sz) != '\0') sz++;

  name = malloc(sz+1);
  memcpy(name, cmd->param, sz+1);

  res = wongate_gethost(gate, name, &newname, ipadr);

  free(name);

  if(res) {
    repl->size = 1 + strlen(newname)+1 + 4;
    wongate_cmd_adjust(repl);
    wongate_cmd_setparam_byte(repl, 0, res);
    memcpy(&repl->param[1], newname, strlen(newname)+1);
    wongate_cmd_setparam_byte(repl, 1+strlen(newname)+1 +0, ipadr[0]);
    wongate_cmd_setparam_byte(repl, 1+strlen(newname)+1 +1, ipadr[1]);
    wongate_cmd_setparam_byte(repl, 1+strlen(newname)+1 +2, ipadr[2]);
    wongate_cmd_setparam_byte(repl, 1+strlen(newname)+1 +3, ipadr[3]);
    free(newname);
  }else{
    repl->size = 1;
    wongate_cmd_adjust(repl);
    wongate_cmd_setparam_byte(repl, 0, res);
  }

  err = 0;
_fail:
  return err;
}

WONFENCE_HANDLER(cmd1101_socknew)
{
  int err = -1;

  if((wongate_cmd_getparam_byte(cmd, 0) != 0x00)) {
    dbgprintf("New socket with bad header!\n");
  }

  repl->size = 1;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, wongate_socknew(gate));

  err = 0;
_fail:
  return err;
}

WONFENCE_HANDLER(cmd1103_sockcon)
{
  int err = -1;
  BYTE s;
  WORD afam;

  s = wongate_cmd_getparam_byte(cmd, 0);
  afam = (wongate_cmd_getparam_byte(cmd, 1) << 8) |
         (wongate_cmd_getparam_byte(cmd, 2) << 0);

  repl->size = 2;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, 0);
  wongate_cmd_setparam_byte(repl, 1, wongate_sockcon(gate, s, afam, &cmd->param[3], cmd->size-3));

  err = 0;
_fail:
  return err;
}

WONFENCE_HANDLER(cmd1106_sockunk)
{
  int err = -1;

  /* TODO: No idea what this does... */
  dbgprintf("SOCKUNK!\n");
  repl->size = 1;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, 1); /* Fake success I guess */

  err = 0;
_fail:
  return err;
}


WONFENCE_HANDLER(cmd1107_sockdel)
{
  int err = -1;

  repl->size = 1;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, wongate_sockdel(gate, wongate_cmd_getparam_byte(cmd, 0)));

  err = 0;
_fail:
  return err;
}

WONFENCE_HANDLER(cmd110E_socksend)
{
  int err = -1;
  BYTE s;

  s = wongate_cmd_getparam_byte(cmd, 0);

  repl->size = 2;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, 0);
  wongate_cmd_setparam_byte(repl, 1, wongate_socksend(gate, s, &cmd->param[1], cmd->size-1));

  err = 0;
_fail:
  return err;
}

WONFENCE_HANDLER(cmd110F_sockrecv)
{
  int err = -1;
  BYTE s, len, ret;
  BYTE tmpbuf[256];

  s = wongate_cmd_getparam_byte(cmd, 0);
  len = wongate_cmd_getparam_byte(cmd, 1);

  ret = wongate_sockrecv(gate, s, tmpbuf, len);

  repl->size = 2;
  if(ret >= 0)
    repl->size += ret;
  wongate_cmd_adjust(repl);
  wongate_cmd_setparam_byte(repl, 0, 0);
  wongate_cmd_setparam_byte(repl, 1, ret);
  if(ret >= 0)
    memcpy(&repl->param[2], tmpbuf, ret);

  err = 0;
_fail:
  return err;
}

#if 0
/* Skeleton handler */
WONFENCE_HANDLER(cmdunknown)
{
  int err = -1;

  repl->size = 0;
  wongate_cmd_adjust(repl);

  err = 0;
_fail:
  return err;
}
#endif

/* Default handler */
WONFENCE_HANDLER(cmdunknown)
{
  int err = -1;

  /* Don't reply */
  repl->zero = 0x80;

  dbgprintf("Unhandled command!\n");

  err = 0;
_fail:
  return err;
}
