#include "top.h"

#include <netdb.h>
#include <sys/socket.h>

#define DEFAULT_RECEPTION 15

wongate_t* wongate_new(void)
{
  wongate_t* gate;

  gate = malloc(sizeof(wongate_t));
  assert(gate != NULL);

  gate->ppp.user = NULL;
  gate->ppp.pass = NULL;

  gate->phone.dialed = NULL;

  gate->cfg = NULL;
  wongate_clear(gate);

  return gate;
}
void wongate_free(wongate_t* gate)
{
  if(gate == NULL)
    return;

  wongate_clear(gate);

  free(gate);
}

void wongate_clear_dns(wongate_t* gate)
{
  gate->dns1 = gate->dns2 = 0;
}
void wongate_clear_pdc(wongate_t* gate)
{
  gate->pdc.reception = DEFAULT_RECEPTION;
  gate->pdc.status = PDCSTAT_NOPDC;
}
void wongate_clear_ppp(wongate_t* gate)
{
  if(gate->ppp.user != NULL) {
    free(gate->ppp.user);
    gate->ppp.user = NULL;
  }
  if(gate->ppp.pass != NULL) {
    free(gate->ppp.pass);
    gate->ppp.pass = NULL;
  }
}
void wongate_clear_phone(wongate_t* gate)
{
  if(gate->phone.dialed != NULL) {
    free(gate->phone.dialed);
    gate->phone.dialed = NULL;
  }
}
void wongate_clear_sockets(wongate_t* gate)
{
  int i;
  for(i = 0; i < FIRST_SOCK; i++) {
    gate->sockets.mapping[i] = -2;
  }
  for(; i < 255; i++) {
    gate->sockets.mapping[i] = -1;
  }
}

void wongate_clear(wongate_t* gate)
{
  gate->ver = WONDERGATE_VERSION;
  gate->status = MWGSTAT_OFF;

  wongate_clear_dns(gate);
  wongate_clear_ppp(gate);

  wongate_clear_pdc(gate);
  wongate_clear_phone(gate);

  wongate_clear_sockets(gate);
}

void wongate_set_config(wongate_t* gate, wongate_cfg_t* cfg)
{
  gate->cfg = cfg;

  if(gate->ppp.user != NULL) {
    free(gate->ppp.user);
    gate->ppp.user = NULL;
  }
  if(gate->ppp.pass != NULL) {
    free(gate->ppp.pass);
    gate->ppp.pass = NULL;
  }

  gate->ppp.user = strdup(gate->cfg->ppp.user);
  gate->ppp.pass = strdup(gate->cfg->ppp.pass);
}

WORD wongate_poweron(wongate_t* gate)
{
  WORD ret = 0;

  if(gate->status == MWGSTAT_OFF) {
    wongate_clear(gate);

    gate->status = MWGSTAT_ON;

    if(gate->cfg != NULL) {
      gate->pdc.reception = gate->cfg->pdc.reception;
    }else{
      gate->pdc.reception = DEFAULT_RECEPTION;
    }
    gate->pdc.status = PDCSTAT_OK;
  }

  ret = ((0x30+((gate->ver >> 4) & 0xF)) << 0) |
        ((0x30+((gate->ver >> 0) & 0xF)) << 8);

  return ret;
}
BYTE wongate_poweroff(wongate_t* gate)
{
  gate->status = MWGSTAT_OFF;

  return 0;
}

BYTE wongate_status(wongate_t* gate)
{
  return gate->status;
}
BYTE wongate_pdc_status(wongate_t* gate)
{
  return gate->pdc.status;
}
BYTE wongate_pdc_reception(wongate_t* gate)
{
  return gate->pdc.reception;
}

BYTE wongate_set_ppp_login(wongate_t* gate, char* user, char* pass)
{
  if(gate->ppp.user != NULL) {
    free(gate->ppp.user);
    gate->ppp.user = NULL;
  }
  if(gate->ppp.pass != NULL) {
    free(gate->ppp.pass);
    gate->ppp.pass = NULL;
  }

  if(user == NULL)
    gate->ppp.user = NULL;
  else
    gate->ppp.user = strdup(user);

  if(pass == NULL)
    gate->ppp.pass = NULL;
  else
    gate->ppp.pass = strdup(pass);

  dbgprintf("# Set PPP login! User: ");
  if(gate->ppp.user) dbgprintf("'%s' ", gate->ppp.user);
  else               dbgprintf("(none) ");
  dbgprintf("Pass: ");
  if(gate->ppp.pass) dbgprintf("'%s'", gate->ppp.pass);
  else               dbgprintf("(none)");
  dbgprintf("\n");

  return PPPSTAT_OK;
}

BYTE wongate_set_dns(wongate_t* gate, BYTE dns1[4], BYTE dns2[4])
{
  BYTE res = 0;

  gate->dns1 = (dns1[0] << 24) |
               (dns1[1] << 16) |
               (dns1[2] <<  8) |
               (dns1[3] <<  0);
  gate->dns2 = (dns2[0] << 24) |
               (dns2[1] << 16) |
               (dns2[2] <<  8) |
               (dns2[3] <<  0);

  res = 1;
  return res;
}

BYTE wongate_dial(wongate_t* gate, char* num)
{
  BYTE res = DIALSTAT_OK;

  if(gate->phone.dialed != NULL) {
    free(gate->phone.dialed);
    gate->phone.dialed = NULL;
  }

  if(num == NULL)
    gate->phone.dialed = NULL;
  else
    gate->phone.dialed = strdup(num);

  dbgprintf("# Dialing ");
  if(gate->phone.dialed) dbgprintf("'%s'", gate->phone.dialed);
  else                   dbgprintf("(none)");
  dbgprintf("\n");

  if(res == DIALSTAT_OK) {
    gate->status = MWGSTAT_PPP;
  }else{
    gate->status = MWGSTAT_ON;
  }

  return res;
}

BYTE wongate_hangup(wongate_t* gate)
{
  BYTE res = DIALSTAT_OK;

  if(gate->phone.dialed != NULL) {
    free(gate->phone.dialed);
    gate->phone.dialed = NULL;
  }

  dbgprintf("# Hanging up.\n");

  gate->status = MWGSTAT_ON;

  return res;
}

BYTE wongate_gethost(wongate_t* gate, char* name, char** newname, uint8_t* ipadr)
{
  BYTE res = 0;
  struct hostent *ent;
  char* rname = NULL;

  if(name == NULL)
    goto _done;
  if(newname == NULL)
    goto _done;

  wongate_HACK_hostname(gate, name, &rname);

  dbgprintf("# Lookup name '%s' (%s):", name, rname);
  ent = gethostbyname(rname);
  if(ent == NULL) {
    dbgprintf(" failed!\n");
    goto _done;
  }else{
    *newname = strdup(ent->h_name);
    ipadr[0] = ent->h_addr[0];
    ipadr[1] = ent->h_addr[1];
    ipadr[2] = ent->h_addr[2];
    ipadr[3] = ent->h_addr[3];
  }

  dbgprintf(" '%s'\n", *newname);

  res = 1;
_done:
  if(rname != NULL)
    free(rname);

  return res;
}

BYTE wongate_socknew(wongate_t* gate)
{
  BYTE res = -1;
  int s;

  s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if(s < 0) {
    dbgprintf("# Creating socket failed.\n");
    goto _done;
  }

  for(res = 0; res < 255; res++) {
    if(gate->sockets.mapping[res] == -1)
      break;
  }
  if(res == 255)
    goto _done;

  gate->sockets.mapping[res] = s;

_done:
  return res;
}

BYTE wongate_sockcon(wongate_t* gate, BYTE s, WORD afam, BYTE* data, BYTE datalen)
{
  BYTE res = 0;
  int rs;
  struct sockaddr* addr;
  struct sockaddr_in addr_in;
  size_t asz;

  if(s == 255)
    goto _done;
  rs = gate->sockets.mapping[s];

  if(rs < 0)
    goto _done;

  switch(afam) {
    case 0: /* AF_UNSPEC */
    case 2: { /* AF_INET */
      memset(&addr_in, 0, sizeof(addr_in));
      addr_in.sin_family = AF_INET;
      /* input data has this in little endian */
      addr_in.sin_port = htons((data[0] << 0) |
                               (data[1] << 8));
      /* input data has this in network endian already */
      addr_in.sin_addr.s_addr = (data[2] <<  0) |
                                (data[3] <<  8) |
                                (data[4] << 16) |
                                (data[5] << 24);

      dbgprintf("# Connecting AF_INET to %08X on port %d ", ntohl(addr_in.sin_addr.s_addr), ntohs(addr_in.sin_port));

      wongate_HACK_socket(gate, &addr_in.sin_addr.s_addr, &addr_in.sin_port);

      dbgprintf("(%08X on port %d)\n", ntohl(addr_in.sin_addr.s_addr), ntohs(addr_in.sin_port));

      addr = (struct sockaddr*)&addr_in;
      asz = sizeof(addr_in);
      break;
    }
    default:
      dbgprintf("# Unknown address family %d for sockcon\n", afam);
      goto _done;
  }

  if(connect(rs, addr, asz) < 0) {
    perror("sockcon");
    dbgprintf("# Connecting to socket (%d %d, %d, %lu) failed.\n", rs, afam, datalen, asz);
    usleep(5000000);
    goto _done;
  }

  res = 1;
_done:
  return res;
}

BYTE wongate_socksend(wongate_t* gate, BYTE s, BYTE* data, BYTE datalen)
{
  BYTE res = -1;
  int rs;

  if(s == 255)
    goto _done;
  rs = gate->sockets.mapping[s];

  if(rs < 0)
    goto _done;

  res = send(rs, data, datalen, 0);

_done:
  return res;
}

BYTE wongate_sockrecv(wongate_t* gate, BYTE s, BYTE* data, BYTE datalen)
{
  BYTE res = -1;
  int rs;

  if(s == 255)
    goto _done;
  rs = gate->sockets.mapping[s];

  if(rs < 0)
    goto _done;

  res = recv(rs, data, datalen, 0);

_done:
  return res;
}

BYTE wongate_sockdel(wongate_t* gate, BYTE s)
{
  BYTE res = 0;
  int rs;

  if(s == 255)
    goto _done;
  rs = gate->sockets.mapping[s];

  if(rs < 0)
    goto _done;

  close(rs);
  gate->sockets.mapping[s] = -1;

  res = 1;
_done:
  return res;
}
