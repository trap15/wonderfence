#include "top.h"

void wongate_HACK_hostname(wongate_t* gate, char* name, char** rname)
{
  int i;
  if(gate->cfg == NULL)
    goto _nohack;
  if(gate->cfg->hosthack_count <= 0)
    goto _nohack;

  for(i = 0; i < gate->cfg->hosthack_count; i++) {
    if(strcmp(name, gate->cfg->hosthacks[i].in_name) == 0) {
      *rname = strdup(gate->cfg->hosthacks[i].out_name);
      return;
    }
  }

_nohack:
  *rname = strdup(name);
}

void wongate_HACK_socket(wongate_t* gate, uint32_t* addr, uint16_t* port)
{
  int i;
  if(gate->cfg == NULL)
    goto _nohack;
  if(gate->cfg->sockhack_count <= 0)
    goto _nohack;

  for(i = 0; i < gate->cfg->sockhack_count; i++) {
    if(*addr != gate->cfg->sockhacks[i].in_ip)
      continue;
    if((gate->cfg->sockhacks[i].in_port != *port) &&
       (gate->cfg->sockhacks[i].in_port != 0))
      continue;

    if(gate->cfg->sockhacks[i].in_port == *port)
      *port = gate->cfg->sockhacks[i].out_port;

    *addr = gate->cfg->sockhacks[i].out_ip;
    return;
  }

_nohack:
  return;
}
