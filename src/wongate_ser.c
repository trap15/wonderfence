#include "top.h"

tser_errcode wongate_ser_recv(tser_t* ts, wongate_cmd_t* cmd)
{
  tser_errcode err = TSER_ERR_UNK;
  WORD i;

  /* Recv header */
  err = tser_recv_byte(ts, &cmd->type);
  if(err < 0) goto _fail;
  err = tser_recv_byte(ts, &cmd->zero);
  if(err < 0) goto _fail;
  err = tser_recv_byte(ts, &cmd->size);
  if(err < 0) goto _fail;

  if(wongate_cmd_adjust(cmd) < 0) {
    err = TSER_ERR_UNK;
    goto _fail;
  }

  if(cmd->size > 0) {
    err = tser_recv_byte(ts, &cmd->cmd);
    if(err < 0) goto _fail;

    cmd->size--;
  }else{
    cmd->cmd = 0;
  }

  /* Recv parameters */
  err = tser_recv(ts, cmd->size, cmd->param);
  if(err < 0) goto _fail;

  dbgprintf("<= Recv cmd: Type:Cmd=%02X:%02X Size=%02X", cmd->type, cmd->cmd, cmd->size);
  if(cmd->size) dbgprintf(" (");
  for(i = 0; i < cmd->size; i++) {
    if(i != 0) dbgprintf(",");
    dbgprintf("%02X", cmd->param[i]);
  }
  if(cmd->size) dbgprintf(")");
  dbgprintf("\n");

_fail:
  return err;
}

tser_errcode wongate_ser_send(tser_t* ts, wongate_cmd_t* cmd)
{
  tser_errcode err = TSER_ERR_UNK;
  WORD i;

  dbgprintf("=> Send cmd: Type:Cmd=%02X:%02X Size=%02X", cmd->type, cmd->cmd, cmd->size);
  if(cmd->size) dbgprintf(" (");
  for(i = 0; i < cmd->size; i++) {
    if(i != 0) dbgprintf(",");
    dbgprintf("%02X", cmd->param[i]);
  }
  if(cmd->size) dbgprintf(")");
  dbgprintf("\n");

  /* Send header */
  err = tser_send_byte(ts, cmd->type);
  if(err < 0) goto _fail;
  err = tser_send_byte(ts, cmd->zero);
  if(err < 0) goto _fail;
  err = tser_send_byte(ts, cmd->size+1);
  if(err < 0) goto _fail;
  err = tser_send_byte(ts, cmd->cmd);
  if(err < 0) goto _fail;

  /* Send parameters */
  err = tser_send(ts, cmd->size, cmd->param);
  if(err < 0) goto _fail;

_fail:
  return err;
}
